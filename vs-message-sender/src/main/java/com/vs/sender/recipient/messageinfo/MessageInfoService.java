package com.vs.sender.recipient.messageinfo;

import com.vs.sender.mailsender.messageinfo.EmailSenderService;
import org.springframework.stereotype.Component;

@Component
class MessageInfoService {

    private final EmailSenderService emailSenderService;

    MessageInfoService(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    void build(MessageInfoDto message) {
        System.out.println("Build message: " + message);
    }

    void save(MessageInfoDto message) {
        System.out.println("Save message: " + message);
    }

    void send(MessageInfoDto message) {
        emailSenderService.sendSimpleMessage(message.getRecipientMail(), message.getTitle(), message.getDescription());
    }
}
