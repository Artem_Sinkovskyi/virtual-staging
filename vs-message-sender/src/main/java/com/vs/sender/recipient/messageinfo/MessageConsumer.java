package com.vs.sender.recipient.messageinfo;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
class MessageConsumer {

    private final MessageInfoService messageInfoService;

    MessageConsumer(MessageInfoService messageInfoService) {
        this.messageInfoService = messageInfoService;
    }

    @RabbitListener(queues = "${vs.gateway.client.service.rabbitmq.messageinfo.queue}")
    void messageInfoListener(MessageInfoDto message) {
        messageInfoService.build(message);
        messageInfoService.save(message);
        messageInfoService.send(message);
    }
}
