package com.vs.sender.mailsender.messageinfo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
class EmailSenderServiceImpl implements EmailSenderService {

    private String sender;

    private final JavaMailSender emailSender;

    EmailSenderServiceImpl(@Value("${spring.mail.sender}") String sender,
                           @Qualifier("vsJavaMailSender") JavaMailSender emailSender) {
        this.sender = sender;
        this.emailSender = emailSender;
    }

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(sender);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);

        emailSender.send(message);
    }
}
