package com.vs.sender.mailsender.messageinfo;

public interface EmailSenderService {

    void sendSimpleMessage(String to, String subject, String text);
}
