package com.vs.api.client.employee;

import com.vs.api.generatedclient.EmployeeApi;
import com.vs.api.generatedclient.model.EmployeeDto;
import com.vs.api.generatedclient.model.IdDto;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;


@Api(tags = {"Employee"})
@RestController
public class EmployeeController implements EmployeeApi {
    @Override
    public ResponseEntity<Void> deactivateEmployeeById(Integer employeeId) {
        return null;
    }

    @Override
    public ResponseEntity<Void> deleteEmployeeById(Integer employeeId) {
        return null;
    }

    @Override
    public ResponseEntity<EmployeeDto> findByEmployeeId(Integer employeeId) {
        return null;
    }

    @Override
    public ResponseEntity<IdDto> saveEmployee(EmployeeDto employeeDto) {
        return null;
    }
}
