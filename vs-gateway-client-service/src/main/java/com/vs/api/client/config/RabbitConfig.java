package com.vs.api.client.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.jackson2.CoreJackson2Module;

@Configuration
public class RabbitConfig {

    @Bean("vsGatewayConnectionFactory")
    public CachingConnectionFactory connectionFactory(@Value("${spring.rabbitmq.host}") String host,
                                                      @Value("${spring.rabbitmq.port}") int port,
                                                      @Value("${spring.rabbitmq.username}") String username,
                                                      @Value("${spring.rabbitmq.password}") String password) {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host, port);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        cachingConnectionFactory.setPublisherReturns(true);
        return cachingConnectionFactory;
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        ObjectMapper objectMapper = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .registerModule(new CoreJackson2Module());
        return new Jackson2JsonMessageConverter(objectMapper);
    }

    @Bean("vsGatewayRabbitTemplate")
    public RabbitTemplate rabbitTemplate(@Qualifier("vsGatewayConnectionFactory") ConnectionFactory vsGatewayConnectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(vsGatewayConnectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}
