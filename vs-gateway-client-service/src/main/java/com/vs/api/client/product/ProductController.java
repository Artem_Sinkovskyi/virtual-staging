package com.vs.api.client.product;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {
    private final ProductAdapter productAdapter;

    public ProductController(ProductAdapter productAdapter) {
        this.productAdapter = productAdapter;
    }

    @GetMapping("/all")
    public List<Product> getAll() {
        return productAdapter.getAll();
    }
}
