package com.vs.api.client.sender.messageinfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
class MessageProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageProducer.class);

    private final String exchange;
    private final String routingKey;

    private final RabbitTemplate vsGatewayRabbitTemplate;

    MessageProducer(@Value("${vs.gateway.client.service.rabbitmq.message.sender.exchange}") String exchange,
                    @Value("${vs.gateway.client.service.rabbitmq.message.sender.routing.key}") String routingKey,
                    @Qualifier("vsGatewayRabbitTemplate") RabbitTemplate vsGatewayRabbitTemplate) {
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.vsGatewayRabbitTemplate = vsGatewayRabbitTemplate;
    }

    @Scheduled(fixedDelay = 100)
    void send() {
        MessageInfoDto testMessageInfoDto = createTestMessageInfoDto();
        System.out.println(testMessageInfoDto);
        try {
            vsGatewayRabbitTemplate.convertAndSend(exchange, routingKey, testMessageInfoDto);
        } catch (AmqpException ex) {
            LOGGER.error("Message '{}' cannot send. {}", "MessageInfo", ex);
        }
    }

    private MessageInfoDto createTestMessageInfoDto() {
        MessageInfoDto messageInfoDto = new MessageInfoDto();

        messageInfoDto.setRecipientMail("<recipient@mail>");
        messageInfoDto.setTitle("Title: " + 1);
        messageInfoDto.setDescription("Description: " + 1);

        return messageInfoDto;
    }
}
