package com.vs.api.client.product;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@Component
public class ProductAdapter {

    private static final String REQUESTS_ENDPOINT = "/product";

    private final String adminPanelUrl;
    private final RestTemplate restTemplate;

    public ProductAdapter(@Value("${vs.admin.panel.api.url}") String adminPanelUrl,
                          RestTemplateBuilder builder) {
        this.adminPanelUrl = adminPanelUrl;
        this.restTemplate = builder.build();
    }

    List<Product> getAll() {
        String path = "/all";
        ResponseEntity<List<Product>> response =
                restTemplate.exchange(adminPanelUrl + REQUESTS_ENDPOINT + path, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }
}
