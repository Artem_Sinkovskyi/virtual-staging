1. go to current folder
2. docker run -it ubuntu
3. apt-get update
4. apt install default-jre
    1. java -version
5. apt install default-jdk
    1. javac -version
6.  (*create file*) cat > Main.java
    ```java
    public class Main {
    
            public static void main(String[] args) {
                    System.out.println("Hello word");
            }
    }
    ```
7. (*check created file*) 
    1. ls
    2. cat Main.java
8.  mkdir  helloword
9. (*copy created file to dir helloword*) cp Main.java /helloword
10. cd helloword
11. javac Main.java
12. java Main


