###Docker

[Docker][1] — программное обеспечение для автоматизации развёртывания и управления приложениями в средах с поддержкой контейнеризации.

Инструменты докер

Назначение | команда
:--- | :---
Docker (CLI) | 1. интерфейс командной строки; <br> 2. работает на хостовой машине; <br> 3. синтаксический сахар над CURL; <br> 4. отправляет команды на Docker Engine;
Docker engine  | 1. не обязательно находится на машине на которой находится Docker CLI; <br> 2. совокупность сервисов, которые отвечают за запуск сервисов в Linux образной системе;<br> 3. являются прообразом микр сервисной архитектур; <br> 4. имеет три главных компонента: <br> &emsp;1) Сервер работающий в фоновом режиме (демон).<br> &emsp;2) REST API, который используют программы для взаимодействия с сервером.<br>&emsp;3) Интерфейс командной строки (CLI) клиент.
Docker Engine (API) | слушатель (listener), который предоставляет REST API и транспортируєт запросы в команды в операционной системе для общения с daemon                                                                                                                         
Docker Engine (daemon) |  процесс, который выделяет для следующего процесса name space и control groups <br> является фоновым процессом, который управляет объектами Docker <br> такими как: образы, контейнеры тома и сети <br> ```docker run -it ubuntu``` <br>```ps aux``` **PID 1**
Docker Engine (containerd) |
Docker Engine (OCI) | запускает процес, который и будет называться контейнером и который с помощью daemon и API с помощью CLI можем передавать на этот контейнер команды и влиять на его жизнеспособность и жизнедеятельность.

```
gradlew build
java -jar virtual-stage-0.0.1-SNAPSHOT.jar
```

```dockerfile
FROM openjdk:11
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
```



```dockerfile
FROM gradle:6.8-jdk11 AS build
COPY --chown=gradle:gradle ../../.. /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle clean build --no-daemon

FROM openjdk:11
EXPOSE 8080
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/*.jar /app/application-name.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar","/app/application-name.jar"]
```

- Образ - шаблон приложения и его зависимостей собранных вместе
- Контейнер - экземпляр образа
- хорошей практикой является указание конкретного тега (так как по умолчанию скачивается latest)
- когда докер создает образы - они строятся по “слоеной”архитектуре (Layered Architecture)
- каждая новая инструкция в Dockerfile создает новый слой, которая хранить только изменения от предыдущего слоя
- все слои кэшируются (при ребилде докер перестраивает только те слои, в которые были внесены изменения) / используется локальный кэш
- у каждого слоя есть свой hash (по которому докер отслеживает их уникальность)

Команда | Назначение
:--- | :---
ipconfig |
cat /etc/*rel* |




Команда | Назначение | Пример
:--- | :--- | :---
```docker version``` | узнать версию докера
```docker run``` | запустить контейнер из образа | docker run nginx sleep 5
```docker inspect``` | узнать информацию о контейнере <br> **Например:** MacAddress / IPAddress | docker inspect webportal
```docker logs``` | посмотреть логи контейнера | docker logs webportal
```docker history``` | посмотреть информацию о слоях образа | docker history webportal
```#history``` | history (выполненая в контейнере) показывает все команды, которые уже были использованы <br> можна использовать как инструкцию для Dockerfile |
```docker images``` | просмотреть список всех доступных на host образов
```docker ps``` | просмотреть список запущенных контейнеров и основную информацию о них
```docker ps -a``` | просмотреть список всех контейнеров
```docker stop``` | остановить работающий контейнер  | docker stop id/name
```docker rm```  | удалить завершенные (**Exited**) и остановленные (**Stopped**) контейнеры <br> в случае успеха вернется id и name контейнера
```docker rm $(docker ps -aq)``` | удалить все контейнеры
```docker rmi ``` | удалить образ <br> **!** перед удалением убедится, что контейнеры от данного образа не запущены в системе| docker rmi nginx
```docker exec ``` | выполнить команду в УЖЕ РАБОТАЮЩЕМ docker контейнере <br> (в данном случае посмотреть содержимое файла) | docker exec clever_morse cat /etc/host
```docker run app``` | запуск контейнера с прикреплением консоли к приложению (CTRL+C для принудительной остановки) | docker run mingrammer/flog flog -d 1 -n 500
```docker run -d app``` |  запуск контейнера с использованием  detached mode <br> запуск контейнера в фоновом режиме | docker run -d  mingrammer/flog flog -d 1 -n 500
```docker attach id/name``` | прикрепить контейнер к косоли (id/name контейнера) | docker attach -d  id/name
```docker run -it id/name``` | ```-it``` - для входа в контейнер (id/name контейнера) <br> ```sh``` - оболочка shall | docker run -it alpine sh
```docker run -p 38123:8080 name:v2```|  ```-p``` - сопоставление портов <br>```8080``` - порт контейнера <br> ```38123``` - порт хоста <br> ```name``` - имя контейнера <br>```v2``` - тег
```docker pull ``` | скачать образ (***без запуска***)
```docker tag id образа ```<br>```repository/name:tag_name``` | ```id образа``` - если создан безымянный образ у  него все равно будет его id <br> ``` repository/name``` - имя пользователя на docker hub / название образа <br> ```tag_name``` - имя тега
```docker login``` | регистрация для возможности использовать push в свой репозиторий на docker hub
```docker push ``` | push образа  в свой репозиторий на docker hub  <br> образы на docker hub хранится в сжатом виде и их размер может отличатся от размера локального образа

###docker run

Команда | Назначение
:--- | :---
```docker run redis``` | запустить контейнер
```docker run redis:5.0``` | запустить контейнер с указанием версии <br> 5.0 - tag <br> если не указать tag - докер будет искать образ и тегом latest
```docker run -t redis``` | ```-t```- использование псевдо терминала
```docker run -i redis``` | ```-i``` -включение интерактивного режима связывая std in хоста и std in контейнера
```docker run -it redis``` | ```-it``` - подключение и взаимодействие с контейнером в интерактивном режиме
```docker run -p 80:5000 webapp``` | ```-p``` - сопоставление портов <br> ```5000``` - порт контейнера <br> ```80``` - порт хоста  <br> весь трафик, полученый на порт 80, будет маршрутизироваться на порт 5000 (внутри контейнера)
```docker run -v /opt/datadir:/var/lib/mysql mysql```<br> ```docker run -v /opt/datadir:/var/lib/mysql -u root mysql```| ```-v``` - (volume) связывание директорий хоста и контейнера ```/opt/datadir``` - директория, что находится на хосте (**откуда**)<br> /var/lib/mysql - директория, что находится в контейнере(**куда**)



Шаги | команды в Dockerfile
:--- | :---
1. Установить операционную систему (Например: OC-Ubuntu)  | FROM ubuntu
2. Обновить apt репозиторий (обновление индекса пакетов) (используя apt update) | RUN apt-get update
3. Установить зависимости apt (в данном случае установка python3 с его менеджером пакетов pip) | RUN apt-get install -y python3 python3-pip
4. Установить зависимости pip (в данном случае установка фреймворка flask) | RUN pip3 install flask
5. Скопировать (из локальной системы ) код в соответствующую папку контейнера (в данном случае в /opt)  | COPY app.py opt/app.py
6. Запустить код app (web-server) используя, спицифичную для 'flask', команду <br> команда ENTRYPOINT сработает когда образ будет запущен в качестве контейнера | ENTRYPOINT FLASK_APP=/opt/app.py flask run --host=0.0.0.0 --port=5000


Dockerfile
CMD | ENTRYPOINT

CMD | ENTRYPOINT | CMD &ENTRYPOINT
:--- | :--- | :---
```FROM Ubuntu```<br> ```CMD sleep 5```  <br> <br> docker run ubuntu-sleeper <br> (*будет выполнятся 5 секунд*) <br> docker run ubuntu-sleeper sleep 10 <br> (*“перекрывает” захаркоженные 5 секунд*) | ```FROM Ubuntu```<br> ```ENTRYPOINT ["sleep"]``` <br> <br>  docker run ubuntu-sleeper 10 <br> (*10 - добавленный параметр*) <br> но если не указать аргумент ```количество секунд``` - ошибка <br> (*нету значения по умолчанию*) |  ```FROM Ubuntu```<br> ```ENTRYPOINT ["sleep"]``` <br> ```CMD sleep 5``` <br> <br>  docker run ubuntu-sleeper <br> (*5 - захардкожденное значение по умолчанию*)<br>  docker run ubuntu-sleeper 10 <br> (*10 - добавленный параметр*)


Хранение в docker


docker run -it ubuntu


apt-get update

docker build . -t rotoro/webapp
docker push
export DOCKER_HOST=00.000.000.00







[1]: (https://ru.wikipedia.org/wiki/Docker)


####Полезные ресурсы
1. [Введение в Docker](https://dker.ru/docs/docker-engine/docker-overview/)
1. [Введение в Docker](https://habr.com/ru/post/487922/)

