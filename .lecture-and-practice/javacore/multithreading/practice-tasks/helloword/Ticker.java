package com.vs.helloword;

public class Ticker implements Runnable {
    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                System.out.println("Hello word: " + i);

                Thread.sleep(1000); //останавливает выполнение потока на колличество ms, которое указанно в его параметрах.
            }
        } catch (InterruptedException e) { // InterruptedException исключение, которое возникает в том случае, если поток прерывается
            e.printStackTrace();
        }
    }
}
