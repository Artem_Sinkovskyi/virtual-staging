###GraphQL

[GraphQL][1] это синтаксис, который описывает как запрашивать данные, и, в основном, используется клиентом для загрузки данных с сервера. GraphQL имеет три основные характеристики:
1. Позволяет клиенту точно указать, какие данные ему нужны.
2. Облегчает агрегацию данных из нескольких источников.
3. Использует систему типов для описания данных.<br>
[Главное преимущество GraphQL][2] — это возможность отправлять запрос, указывающий только ту информацию, которая вам нужна, и получать именно ее.

```
POST: http://localhost:8083/apis/graphql
```
QUERY
```{
    findAllTutorials{
        id
        title
        author {
            id
            name
        }
    }
}
```


[1]:(https://habr.com/ru/post/326986/)
[2]:(https://medium.com/devschacht/esteban-herrera-5-reasons-you-shouldnt-use-graphql-bae94ab105bc)

####Полезные ресурсы
1. [GraphQL with MongoDB (Spring Boot)](https://www.youtube.com/watch?v=EckXWhmgO_I&ab_channel=TALHAJAHANGIRIKHAN)
2. [Примеры GraphQL на Java для начинающих (со Spring Boot)](https://habr.com/ru/post/513170/)
3. [Spring Boot + GraphQL + MongoDB example with Spring Data & graphql-java](https://bezkoder.com/spring-boot-graphql-mongodb-example-graphql-java/)
4. [Spring Boot + GraphQL Hello World Example](https://www.javainuse.com/spring/graphql/hello)
4. [GraphQL](http://spec.graphql.org/draft/#sec-Scalars)