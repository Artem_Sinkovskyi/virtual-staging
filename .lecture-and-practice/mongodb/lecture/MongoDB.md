###NoSql. MongoDB.
[NoSql][1]

####Преимущества
- лучшая масштабируемость
- хранение неструктурированной информации
- быстрая разработка
- низкий входной порог
####Недостатки
- сильная завязка на одну СУБД
- собственный язык запросов
- проще допустить ошибки в проектировании
- часто слабые ACID - свойства

**MongoDB** - документо ориентированная система управления базами данных(СУБД) c открытым исходным кодом, не требующая описания схемы таблиц.
- классифицирована как NoSql
- использует JSON - подобные документы и схемы баз данных
- при подключении к MongoDB хостов может быть больше одного (преимущества масштабируемости)


1. Скачать и Установить MongoDB с официального сайта
2. Создайте папку c:\data\db
3. Выполните "C:\Program Files\MongoDB\Server\{***версия MongoDB***}\bin\mongod.exe" --dbpath "c:\data\db"
4. Выполните "C:\Program Files\MongoDB\Server\{***версия MongoDB***}\bin\mongo.exe" <br>
Или больше информации перейдя по ссылке [MongoDB Guides > Install MongoDB] [2]

####Создание базы данных

Назначение | команда
:--- | :---
Создания Бд / Подключение к Бд | ```> use messenger```
Создание коллекции | ```> db.createCollection("users")```
Удаление коллекции | ```> db.users.drop()```
Вставка одной записи | ```> db.users.insertOne({"birthday":new Date(1991-03-29)})```
Вставка с указанием _id | ```> db.users.insertOne({_id:2, "name":"TEST_NAME"})```
Вставка батча | ```> db.users.insertMany([{"name":"NAME_1"}, {"name":"NAME_2"}])```
Выборка всех элементов | ```> db.users.find()```
Выборка элементов с указанием количества | ```> db.users.find().limit(2)```
Выборка элементов с **БЕЗ** поля **_id** | ```> db.users.find({}, {"_id":0})```
Выборка с сортировкой ( **1 - ASC** / **-1 - DESC**) | ```> db.users.find().sort({age: 1})```
Выборка с сортировкой по нескольким полям  | ```> db.users.find().sort({age: 1, name:-1})```
Сортировка с лимитом | ```> db.users.find().sort({age: 1, name:-1}).limit(2)```
Фильтр по полю дочернего элемента | ```> db.users.find({ "address.zip" : "90404" })```
Фильтр по нескольким полям **AND ==**| ```> db.users.find({ name:"NAME", "address.zip" : "90404" })```
Фильтр по нескольким полям **OR ==**| ```> db.users.find({$or:[{name:"Yana"}, {"zip":"90404"}]})```
Фильтр по нескольким полям **OR ==**| ```> db.users.find({$or:[{name:{$eq:"Yana"}}, {"zip":"90404"}]})```
Фильтр по нескольким полям **OR <** (***$lt*** - меньше)| ```> db.users.find({$or:[{age:{$lt:38}}}, {"zip":"90404"}]})```
Фильтр по нескольким полям **OR <=** (***$lte*** - меньше или равно)| ```> db.users.find({$or:[{age:{$lte:38}}}, {"zip":"90404"}]})```
Фильтр по нескольким полям **OR >** (***$gt*** - больше)| ```> db.users.find({$or:[{age:{$gt:38}}}, {"zip":"90404"}]})```
Фильтр по нескольким полям **OR >=** (***$gtе*** - больше или равно)| ```> db.users.find({$or:[{age:{$gte:38}}}, {"zip":"90404"}]})```
Фильтр по нескольким полям **OR IN** (***$in*** - больше или равно)| ```> db.users.find({$or:[{age:{$in:[38, 39, 45]}} , {"zip":"90404"}]})```
Фильтр по нескольким полям **OR Not IN** (***$nin*** - больше или равно)| ```> db.users.find({$or:[{age:{$nin:[38, 39, 45]}} , {"zip":"90404"}]})```
Фильтр **IF EXISTS / NOT EXISTS FIELD** (***$exists*** - для поля)| ```> db.users.find({{age:{$exists:true}})```
Фильтр **ARRAY SIZE** (***$size*** - количество єлементов в массиве)| ```> db.users.find({{favouriteColours:{$size:2}})```
Фильтр **ARRAY WHERE ELEMENT MATCH MORE THEN SYMBOL 'A'** | ```> db.users.find({{favouriteColours:{$elemMatch:{$lte:"a"}}})```
Обновление **ТОЛЬКО ОДНОЙ** записи  | ```> db.users.updateOne({age:22}, {$set: {age:26}})```
Обновление **ВСЕХ** записей  (первый объект - фильтр)| ```> db.users.updateMany({age:22}, {$set: {age:26}})```
Обновление **ВСЕХ** записей  (первый объект - фильтр)| ```> db.users.updateMany({age:22}, {$set: {age:26, name:"USER"}})```
Замена объекта | ```> db.users.replaceOne({age:22}, {age:26, name:"USER"})```
Удаление **УДАЛЕНИЕ ОДНОГО ОБЪЕКТА** | ```> db.users.deleteOne({age:22})```
Удаление **УДАЛЕНИЕ МНОГИХ ОБЪЕКТОВ** | ```> db.users.deleteMany({age:22})```
Создание индексов для поиска по "text" | ```> db.users.createIndex({biography: "text"})```
Поиск записей по фрагментам ***"text contain any word from phrase"*** | ```> db.users.find({$text: {$search: "any phrase(any worlds)"}})```
Поиск записей с учетом релевантности (совпадения) | ```> db.users```<br>```.find({$text: {$search: "any phrase"}}, {score:{$meta:"textScore"}})```<br>```.sort({score:{$meta:"textScore"}})```
Узнать количество объектов в коллекции по определенному фильтру | ```> db.users.count({age:22})```
Получить **ТОЛЬКО УНИКАЛЬНЫЕ ЗНАЧЕНИЯ** по полю | ```> db.users.count("email")```
Объединение данных (агрегация)  | ```> db.users.aggregate([```<br>&emsp;```{$match: {name: "Mike"}},```<br>&emsp;```{$group: {_id: "$name", $products_bued: {$sum:"$products_bued"}}},```<br>```])```
Объединение данных (агрегация)  | ```> db.users.aggregate([```<br>&emsp;```{$match: {}},```<br>&emsp;```{$group: {_id: "$name", $products_bued: {$sum:"$products_bued"}}},```<br>```])```


**Объединение нескольких команд:**
```
db.users.bulkWrite([
   {
       insertOne: {
           "document" :{name:"TEST_NAME_1", age:43}
       }
   },
   {
       deleteOne: {
          filter: {age : 20}
       }
   },
   {
       updateMany: {
          filter: {name:"TEST_NAME_1"},
          update: {$set: {name:"UPDATED_NAME"}}
       }
   },
   {
       replaceOne: {
          filter: {name:"TEST_NAME_2"},
          replacement: {$set: {name:"REPLACED_NAME"}}
       }
   },
])
```
**Поиск на совпадение в тексте:**
1) НЕобходимо создать индексы по тем полям, по которым будет происходить поиск

1. коллекция - коллекция объектов
2. объекты могут иметь разные наборы данных.
3. коллекция будет создана автоматически если в коллекцию (которой не существует) поместить новый объект
4. любой объект в коллекции - **JSON** объект
5. **_id** - обязательное поле (данное значение генерируется автоматически или задается вручную)
6. коллекция очень эластична, и объекты в ней могут иметь разное количество полей
7. при неправильном написании запроса может возникнуть исключение [SyntaxError: missing : after property id][3]
8. команды:
  1. **$lt** - less than,
  2. **$gt** - greater than,
  3. **$lte / $gte** - less than or equals / greater than or equals
  4. **$eq** - equals
  5. **$ne** - no equals
  6. **$in / $nin** - in range / not in rang
  7. **$exists** - for field
  8. **$size**
  9. **$elemMatch**
  10. **$exists** (for fields)
  
###Main steps
- add to dependencies (***build.gradle***) actual version spring-boot-starter-data-mongodb
- add DB name to application.properties
  - spring.data.mongodb.database
- create persistent class with
  - MongoTemplate
```java
   @Autowired
   private MongoTemplate mongoTemplate;

   public List<ChatMessage> findAll() {
       return mongoTemplate.findAll(ChatMessage.class);
   }
```
- Using MongoDB we operate with collections in which objects are stored
  - *@Document* (**mark entity**)
  - *@Id*
   - *@Field*
  
- extends from MongoRepository can use instead of MongoTemplate
```java
public interface MessageRepository extends MongoRepository<ChatMessage, Integer> {
}
```
**WHERE**
```java
interface MongoRepository<T, ID> extends PagingAndSortingRepository<T, ID>, QueryByExampleExecutor<T>

interface PagingAndSortingRepository<T, ID> extends CrudRepository<T, ID>
```


[1]: (https://ru.wikipedia.org/wiki/NoSQL#:~:text=%D0%A2%D0%B5%D1%80%D0%BC%D0%B8%D0%BD%20%C2%ABBASE%C2%BB%20%D0%B1%D1%8B%D0%BB%20%D0%BF%D1%80%D0%B5%D0%B4%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%20%D0%AD%D1%80%D0%B8%D0%BA%D0%BE%D0%BC,%D0%B4%D0%BE%D1%81%D1%82%D1%83%D0%BF%D0%BD%D0%BE%D1%81%D1%82%D1%8C%20%D0%B8%D0%BB%D0%B8%20%D1%83%D1%81%D1%82%D0%BE%D0%B9%D1%87%D0%B8%D0%B2%D0%BE%D1%81%D1%82%D1%8C%20%D0%BA%20%D1%80%D0%B0%D0%B7%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D1%8E.)
[2]: (https://docs.mongodb.com/guides/server/install/)
[3]: (https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Errors/Missing_colon_after_property_id)



####Полезные ресурсы
1. [Уроки MongoDB для начинающих](https://www.youtube.com/playlist?list=PL0lO_mIqDDFXcxN3fRjc-EOWZLqW8dLVV)
2. [Шардинг и репликация](https://ruhighload.com/%D0%A8%D0%B0%D1%80%D0%B4%D0%B8%D0%BD%D0%B3+%D0%B8+%D1%80%D0%B5%D0%BF%D0%BB%D0%B8%D0%BA%D0%B0%D1%86%D0%B8%D1%8F)
3. [Для чего идеальна MongoDb? Примеры приложений, где монга будет лучше mysql? — Хабр Q&A](https://qna.habr.com/q/475876)
4. [MongoDB - Java](https://coderlessons.com/tutorials/bazy-dannykh/uchitsia-mongodb/mongodb-java)



