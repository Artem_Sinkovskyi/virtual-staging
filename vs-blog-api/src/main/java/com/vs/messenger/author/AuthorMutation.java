package com.vs.messenger.author;

import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class AuthorMutation implements GraphQLMutationResolver {
    private final AuthorRepository authorRepository;

    public Author createAuthor(String name, Integer age) {
        Author author =
                Author.builder()
                        .name(name)
                        .age(age)
                        .build();

        return authorRepository.save(author);
    }
}
