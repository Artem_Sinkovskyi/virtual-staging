package com.vs.messenger.tutorial;

import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class TutorialQuery implements GraphQLQueryResolver {
    private final TutorialRepository tutorialRepository;

    public Iterable<Tutorial> findAllTutorials() {
        return tutorialRepository.findAll();
    }
}
