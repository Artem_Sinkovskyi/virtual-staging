package com.vs.messenger.tutorial;

import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@AllArgsConstructor
@Component
public class TutorialMutation implements GraphQLMutationResolver {

    private final TutorialRepository tutorialRepository;


    public Tutorial createTutorial(String title, String description, String authorId) {
        Tutorial tutorial =
                Tutorial.builder()
                        .authorId(authorId)
                        .title(title)
                        .description(description)
                        .build();

        return tutorialRepository.save(tutorial);
    }

    public boolean deleteTutorial(String id) {
        tutorialRepository.deleteById(id);
        return true;
    }

    public Tutorial updateTutorial(String id, String title, String description) throws Exception {
        Optional<Tutorial> optTutorial = tutorialRepository.findById(id);

        if (optTutorial.isPresent()) {
            Tutorial tutorial = optTutorial.get();

            if (title != null)
                tutorial.setTitle(title);
            if (description != null)
                tutorial.setDescription(description);

            tutorialRepository.save(tutorial);
            return tutorial;
        }

        throw new Exception("Not found Tutorial to update!");
    }
}
