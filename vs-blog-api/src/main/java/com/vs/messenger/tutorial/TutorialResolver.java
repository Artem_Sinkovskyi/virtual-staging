package com.vs.messenger.tutorial;

import com.vs.messenger.author.Author;
import com.vs.messenger.author.AuthorRepository;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class TutorialResolver implements GraphQLResolver<Tutorial> {
    private final AuthorRepository authorRepository;

    public Author getAuthor(Tutorial tutorial) {
        return authorRepository.findById(tutorial.getAuthorId()).orElseThrow(null);
    }
}