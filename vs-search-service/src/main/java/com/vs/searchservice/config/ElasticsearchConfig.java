package com.vs.searchservice.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@Primary
public class ElasticsearchConfig {

    @Value("${vs.search.service.elasticsearch.host}")
    private String host;

    @Value("${vs.search.service.elasticsearch.port}")
    private int port;

    @Value("${vs.search.service.elasticsearch.scheme}")
    private String scheme;

    @Bean(name = "elasticSearchHighLevelClient", destroyMethod = "close")
    public RestHighLevelClient elasticsearchClient() {
        return new RestHighLevelClient(RestClient.builder(new HttpHost(host, port, scheme)));
    }
}
