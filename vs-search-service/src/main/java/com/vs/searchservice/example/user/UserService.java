package com.vs.searchservice.example.user;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public List<User> getAllByName(String userName) {
        return userRepository.findAllByName(userName);
    }

    public List<User> getAllByNameAndAddress(String userName, String address) {
        return userRepository.findAllByNameAndAddress(userName, address);
    }
}
