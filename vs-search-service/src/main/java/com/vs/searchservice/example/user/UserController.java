package com.vs.searchservice.example.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/all")
    public List<User> getAllUser() {
        return userService.getAll();
    }

    @GetMapping(value = "/{userName}")
    public List<User> getUserByName(@PathVariable String userName) {
        return userService.getAllByName(userName);
    }

    @GetMapping(value = "/{userName}/{address}")
    public List<User> getUserByNameAndAddress(@PathVariable String userName,
                                              @PathVariable String address) {
        return userService.getAllByNameAndAddress(userName, address);
    }
}