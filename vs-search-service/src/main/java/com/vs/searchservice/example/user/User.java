package com.vs.searchservice.example.user;

import com.vs.searchservice.example.util.model.ElasticSearchEntity;

public class User implements ElasticSearchEntity {

    private String userId;
    private String userName;
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
