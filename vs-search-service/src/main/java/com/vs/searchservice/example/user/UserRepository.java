package com.vs.searchservice.example.user;

import com.vs.searchservice.example.util.ElasticSearchClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserRepository {

    public static final String USERMANAGE = "usermanage";

    private  final ElasticSearchClient elasticSearchClient;

    public UserRepository(ElasticSearchClient elasticSearchClient) {
        this.elasticSearchClient = elasticSearchClient;
    }

    public List<User> findAll() {
        SearchSourceBuilder query = new SearchSourceBuilder().query(
                QueryBuilders
                        .matchAllQuery());

        return elasticSearchClient.getElasticSearchEntitiesByQuery(query, User.class, USERMANAGE)
                .stream()
                .map(elasticSearchEntity -> (User) elasticSearchEntity)
                .collect(Collectors.toList());
    }

    public List<User> findAllByName(String userName) {
        SearchSourceBuilder query = new SearchSourceBuilder().query(
                QueryBuilders
                        .boolQuery()
                        .must(QueryBuilders.termQuery("userName.keyword", userName)));

        return elasticSearchClient.getElasticSearchEntitiesByQuery(query, User.class, USERMANAGE)
                .stream()
                .map(elasticSearchEntity -> (User) elasticSearchEntity)
                .collect(Collectors.toList());
    }

    public List<User> findAllByNameAndAddress(String userName, String address) {
        SearchSourceBuilder query = new SearchSourceBuilder().query(
                QueryBuilders
                        .boolQuery()
                        .must(QueryBuilders.termQuery("userName.keyword", userName))
                        .must(QueryBuilders.matchQuery("address", address)));

        return elasticSearchClient.getElasticSearchEntitiesByQuery(query, User.class, USERMANAGE)
                .stream()
                .map(elasticSearchEntity -> (User) elasticSearchEntity)
                .collect(Collectors.toList());
    }
}
