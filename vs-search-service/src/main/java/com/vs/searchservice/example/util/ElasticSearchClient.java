package com.vs.searchservice.example.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vs.searchservice.example.util.model.ElasticSearchEntity;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ElasticSearchClient {

    private final ObjectMapper objectMapper;
    private final RestHighLevelClient client;

    public ElasticSearchClient(ObjectMapper objectMapper,
                               @Qualifier("elasticSearchHighLevelClient") RestHighLevelClient client) {
        this.objectMapper = objectMapper;
        this.client = client;
    }

    public List<? super ElasticSearchEntity> getElasticSearchEntitiesByQuery(SearchSourceBuilder sourceBuilder,
                                                                             Class<? extends ElasticSearchEntity> entityClass,
                                                                             String... indices) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(indices);
        searchRequest.source(sourceBuilder);

        List<? super ElasticSearchEntity> elasticSearchEntities = new ArrayList<>();
        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            if (searchResponse.getHits().getTotalHits().value > 0) {
                SearchHit[] searchHit = searchResponse.getHits().getHits();
                for (SearchHit hit : searchHit) {
                    Map<String, Object> map = hit.getSourceAsMap();
                    elasticSearchEntities.add(objectMapper.convertValue(map, entityClass));
                }
            }
        } catch (IOException e) {
            log.error("Search by query: {} in indices {} was incorrect. {}", sourceBuilder, indices, e.getLocalizedMessage());
        }

        return elasticSearchEntities;
    }
}
