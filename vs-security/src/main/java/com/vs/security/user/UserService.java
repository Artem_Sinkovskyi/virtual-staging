package com.vs.security.user;

import java.util.List;

public interface UserService {

    User register(User user);

    List<User> getAll();

    User findByUserName(String userName);

    User findById(Integer id);

    void delete(Integer id);
}
