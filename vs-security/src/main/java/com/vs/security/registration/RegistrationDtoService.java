package com.vs.security.registration;


import com.vs.security.authorization.dto.UserTokenDto;
import com.vs.security.registration.dto.UserRegistrationDto;
import com.vs.security.security.jwt.JwtTokenProvider;
import com.vs.security.user.User;
import com.vs.security.user.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
public class RegistrationDtoService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;

    public RegistrationDtoService(AuthenticationManager authenticationManager,
                                  JwtTokenProvider jwtTokenProvider,
                                  UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    UserTokenDto register(UserRegistrationDto userRegistrationDto) {
        try {
            User user = userService.register(convertToUser(userRegistrationDto));
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));

            return UserTokenDto.builder()
                    .username(user.getUsername())
                    .token(jwtTokenProvider.createToken(user.getUsername(), user.getRoles()))
                    .build();

        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    private User convertToUser(UserRegistrationDto userRegistrationDto) {
        return User.builder()
                .username(userRegistrationDto.getUsername())
                .firstName(userRegistrationDto.getFirstName())
                .lastName(userRegistrationDto.getLastName())
                .email(userRegistrationDto.getEmail())
                .password(userRegistrationDto.getPassword())
                .build();
    }
}
