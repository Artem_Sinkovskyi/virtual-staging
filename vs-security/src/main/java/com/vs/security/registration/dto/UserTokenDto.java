package com.vs.security.registration.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class UserTokenDto {

    private final String username;
    private final String token;
}
