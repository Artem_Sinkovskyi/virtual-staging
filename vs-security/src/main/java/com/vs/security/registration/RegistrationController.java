package com.vs.security.registration;


import com.vs.security.authorization.dto.UserTokenDto;
import com.vs.security.registration.dto.UserRegistrationDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.vs.security.config.ApiV1Matchers.REGISTRATION_ENDPOINT;

@RestController
@RequestMapping(REGISTRATION_ENDPOINT)
public class RegistrationController {

    private final RegistrationDtoService registrationDtoService;

    public RegistrationController(RegistrationDtoService registrationDtoService) {
        this.registrationDtoService = registrationDtoService;
    }

    @PostMapping
    public ResponseEntity<UserTokenDto> registration(@RequestBody UserRegistrationDto userRegistrationDto) {
        return ResponseEntity.ok(registrationDtoService.register(userRegistrationDto));
    }
}
