package com.vs.security.registration.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserRegistrationDto {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
