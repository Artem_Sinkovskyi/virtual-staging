package com.vs.security.admin.user.dto;

import com.vs.security.base.model.Status;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class UserAdminDto {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Status status;
}
