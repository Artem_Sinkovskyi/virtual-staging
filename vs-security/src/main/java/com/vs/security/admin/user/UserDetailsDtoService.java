package com.vs.security.admin.user;

import com.vs.security.admin.user.dto.UserAdminDto;
import com.vs.security.base.model.Status;
import com.vs.security.user.User;
import com.vs.security.user.UserService;
import org.springframework.stereotype.Service;


@Service
public class UserDetailsDtoService {

    private final UserService userService;

    public UserDetailsDtoService(UserService userService) {
        this.userService = userService;
    }

    UserAdminDto getById(Integer id) {
        User user = userService.findById(id);

        return convertToDto(user);
    }

    private UserAdminDto convertToDto(User user) {
        return UserAdminDto.builder()
                .username(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .password(user.getPassword())
                .status(user.getStatus())
                .build();
    }
}
