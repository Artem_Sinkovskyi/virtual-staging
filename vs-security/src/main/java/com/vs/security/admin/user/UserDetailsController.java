package com.vs.security.admin.user;


import com.vs.security.admin.user.dto.UserAdminDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.vs.security.config.ApiV1Matchers.ADMIN_ENDPOINT;


@RestController
@RequestMapping(ADMIN_ENDPOINT)
public class UserDetailsController {

    private final UserDetailsDtoService userDetailsDtoService;

    public UserDetailsController(UserDetailsDtoService userDetailsDtoService) {
        this.userDetailsDtoService = userDetailsDtoService;
    }

    @GetMapping("users/{id}")
    public ResponseEntity<UserAdminDto> getEntityId(@PathVariable Integer id) {
        return ResponseEntity.ok(userDetailsDtoService.getById(id));
    }
}
