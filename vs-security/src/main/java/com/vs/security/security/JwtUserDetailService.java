package com.vs.security.security;

import com.vs.security.security.jwt.JwtUserCreator;
import com.vs.security.user.User;
import com.vs.security.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
@Slf4j
public class JwtUserDetailService implements UserDetailsService {

    private final UserService userService;

    public JwtUserDetailService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUserName(username);

        if (isNull(user)) {
            throw new UsernameNotFoundException(String.format("User with username %s  not found", username));
        }

        return JwtUserCreator.create(user);
    }
}
