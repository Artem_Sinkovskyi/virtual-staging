package com.vs.security.authorization;


import com.vs.security.authorization.dto.AuthenticationRequestDto;
import com.vs.security.authorization.dto.UserTokenDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.vs.security.config.ApiV1Matchers.LOGIN_ENDPOINT;

@RestController
@RequestMapping(LOGIN_ENDPOINT)
public class AuthenticationController {

    private final AuthenticationDtoService authenticationDtoService;

    public AuthenticationController(AuthenticationDtoService authenticationDtoService) {
        this.authenticationDtoService = authenticationDtoService;
    }

    @PostMapping("login")
    public ResponseEntity<UserTokenDto> login(@RequestBody AuthenticationRequestDto requestDto) {
        return ResponseEntity.ok(authenticationDtoService.getUser(requestDto));
    }
}
