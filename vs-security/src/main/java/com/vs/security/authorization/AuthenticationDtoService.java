package com.vs.security.authorization;


import com.vs.security.authorization.dto.AuthenticationRequestDto;
import com.vs.security.authorization.dto.UserTokenDto;
import com.vs.security.security.jwt.JwtTokenProvider;
import com.vs.security.user.User;
import com.vs.security.user.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
class AuthenticationDtoService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;

    AuthenticationDtoService(AuthenticationManager authenticationManager,
                             JwtTokenProvider jwtTokenProvider,
                             UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    UserTokenDto getUser(AuthenticationRequestDto requestDto) {
        try {
            String userName = requestDto.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, requestDto.getPassword()));
            User user = userService.findByUserName(userName);

            if (isNull(user)) {
                throw new UsernameNotFoundException("User with username " + userName + " not found");
            }

            return UserTokenDto.builder()
                    .username(userName)
                    .token(jwtTokenProvider.createToken(userName, user.getRoles()))
                    .build();

        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }
}
