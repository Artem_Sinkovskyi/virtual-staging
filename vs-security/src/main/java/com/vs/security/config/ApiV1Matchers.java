package com.vs.security.config;

public interface ApiV1Matchers {

    String REGISTRATION_ENDPOINT = "/api/v1/registration/";
    String LOGIN_ENDPOINT = "/api/v1/auth/";
    String ADMIN_ENDPOINT = "/api/v1/admin/";
}
