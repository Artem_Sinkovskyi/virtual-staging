package com.vs.security.base.model;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
