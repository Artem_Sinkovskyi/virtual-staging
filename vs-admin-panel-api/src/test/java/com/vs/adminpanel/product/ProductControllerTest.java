package com.vs.adminpanel.product;


import com.google.gson.JsonArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ContextConfiguration(classes = ProductController.class)
@TestExecutionListeners(MockitoTestExecutionListener.class)
@WebMvcTest
public class ProductControllerTest extends AbstractTestNGSpringContextTests {

    @MockBean
    private ProductService productService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void expectedEmptyList_when_anyProductsNotFound() throws Exception {
        when(productService.getAll()).thenReturn(List.of());

        this.mockMvc.perform(get("/product/all"))
                .andExpect(status().isOk())
                .andExpect(content().json(new JsonArray().toString()));
    }

}