package com.vs.adminpanel.product;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        products.add(new Product(1, "TEST_1"));
        products.add(new Product(2, "TEST_2"));
        products.add(new Product(3, "TEST_3"));
        products.add(new Product(4, "TEST_4"));
        return products;
    }
}
